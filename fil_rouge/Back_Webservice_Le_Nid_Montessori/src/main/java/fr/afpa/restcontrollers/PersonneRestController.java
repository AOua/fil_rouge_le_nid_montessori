package fr.afpa.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.metier.entities.Personne;
import fr.afpa.metier.iservices.IServicePersonneMetier;

@RestController
@CrossOrigin(value ="http://localhost:3000")
public class PersonneRestController {

	@Autowired
	private IServicePersonneMetier servicePersonne;
	
	@GetMapping(value = "/personnes")
	public List<Personne> getPersonnes() {
		return servicePersonne.findAll();
		}
	
	@GetMapping("/personnes/{id}")
	public ResponseEntity<?> getPersonne(@PathVariable("id") Integer id,@RequestHeader HttpHeaders header){
					
		HttpStatus status;
		Personne personne = servicePersonne.findOne(id);
		if (personne != null) {
			status = HttpStatus.OK;
		} else {
			status = HttpStatus.BAD_REQUEST;
		}
		return ResponseEntity.status(status).body(personne);

	}
	
	@PostMapping("/personnes")
	public Personne addPersonne(@RequestBody Personne personne) {
	return servicePersonne.addPersonne(personne);
	}

	@PutMapping("/personnes")
	public Personne activerPersonne(@RequestBody Personne personne) {
		return servicePersonne.activerPersonne(personne);
	}
	
}
