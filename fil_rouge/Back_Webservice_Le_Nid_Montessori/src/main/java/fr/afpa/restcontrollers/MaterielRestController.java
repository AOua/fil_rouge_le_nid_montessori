package fr.afpa.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.metier.entities.Materiel;
import fr.afpa.metier.iservices.IServiceMaterielMetier;

@RestController
@CrossOrigin(value ="http://localhost:3000")
public class MaterielRestController {
	
	@Autowired
	private IServiceMaterielMetier serviceMateriel;
	
	@GetMapping(value = "/materiels")
	public List<Materiel> getMateriels() {
		return serviceMateriel.findAll();
		}
	
	@GetMapping("/materiels/{id}")
	public ResponseEntity<?> getMateriel(@PathVariable("id") Integer id,@RequestHeader HttpHeaders header){
					
		HttpStatus status;
		Materiel materiel = serviceMateriel.findOne(id);
		if (materiel != null) {
			status = HttpStatus.OK;
		} else {
			status = HttpStatus.BAD_REQUEST;
		}
		return ResponseEntity.status(status).body(materiel);

	}
	
	@PostMapping("/materiels")
	public Materiel addMateriel(@RequestBody Materiel materiel) {
	return serviceMateriel.addMateriel(materiel);
	}


}
