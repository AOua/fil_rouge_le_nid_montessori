package fr.afpa.metier.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Materiel {

	Integer idMateriel;
	String nomMateriel;
	boolean disponible;
}
