package fr.afpa.metier.entities;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Atelier {

	Integer idAtelier;
	String nomAtelier;
	boolean disponible;
	TypeAtelier typeAtelier;
	List<Reservation> listeReservation;
}
