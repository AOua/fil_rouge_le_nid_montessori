package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dto.iservices.IServiceMaterielDto;
import fr.afpa.metier.entities.Materiel;
import fr.afpa.metier.iservices.IServiceMaterielMetier;

@Service
public class ServiceMaterielMetier implements IServiceMaterielMetier{

	@Autowired
	private IServiceMaterielDto serviceMaterielDto;
	
	@Override
	public List<Materiel> findAll() {
		return serviceMaterielDto.findAll();
	}

	@Override
	public Materiel findOne(Integer id) {
		return serviceMaterielDto.findOne(id);
		}

	@Override
	public Materiel addMateriel(Materiel materiel) {
		return serviceMaterielDto.addMateriel(materiel);
		}

}