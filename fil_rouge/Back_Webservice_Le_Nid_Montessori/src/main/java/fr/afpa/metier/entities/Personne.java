package fr.afpa.metier.entities;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class Personne {

	Integer id;
	String nom;
	String prenom;
	LocalDate dateDeNaissance;
	String mail;
	String tel;
	String adresse;
	boolean actif;
	Role role;
	Authentification authentification;
}
