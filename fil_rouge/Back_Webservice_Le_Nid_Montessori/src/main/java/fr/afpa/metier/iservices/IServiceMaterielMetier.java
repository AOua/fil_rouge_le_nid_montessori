package fr.afpa.metier.iservices;

import java.util.List;

import fr.afpa.metier.entities.Materiel;

public interface IServiceMaterielMetier {

	public List<Materiel> findAll();

	public Materiel findOne(Integer id);

	public Materiel addMateriel(Materiel materiel);
}
