package fr.afpa.metier.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dto.iservices.IServicePersonneDto;
import fr.afpa.metier.entities.Personne;
import fr.afpa.metier.iservices.IServicePersonneMetier;

@Service
public class ServicePersonneMetier implements IServicePersonneMetier {
	
	@Autowired
	private IServicePersonneDto servicePersonneDto;

	@Override
	public List<Personne> findAll() {
		return servicePersonneDto.findAll();
	}

	@Override
	public Personne findOne(Integer id) {
		return servicePersonneDto.findOne(id);
	}

	@Override
	public Personne addPersonne(Personne personne) {
		return servicePersonneDto.addPersonne(personne);
	}

	@Override
	public Personne activerPersonne(Personne personne) {
		return servicePersonneDto.activerPersonne(personne);
		}
	
}
