package fr.afpa.dao.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Data

@Entity
@Table(name = "authentification")
public class AuthentificationDao {

	@Id
	@Column (name="login", updatable = false, nullable = false, length = 50 )
	String login;
	
	@Column (name="mdp", updatable = true, nullable = false,length = 50)
	String mdp;
	
	@OneToOne (mappedBy = "authentification")
	PersonneDao personne;
}
