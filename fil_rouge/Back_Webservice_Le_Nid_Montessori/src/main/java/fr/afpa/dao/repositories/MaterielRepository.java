package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.dao.entities.MaterielDao;

public interface MaterielRepository extends JpaRepository<MaterielDao, Integer>{

}
