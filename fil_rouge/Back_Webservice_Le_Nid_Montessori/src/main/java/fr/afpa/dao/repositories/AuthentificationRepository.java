package fr.afpa.dao.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.dao.entities.AuthentificationDao;

public interface AuthentificationRepository extends JpaRepository<AuthentificationDao, Integer>{
	
	Optional<AuthentificationDao> findByLogin(String login);

}
