package fr.afpa.dao.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity


@Table(name = "atelier")
public class AtelierDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "atelier_generator")
	@SequenceGenerator(name = "atelier_generator", sequenceName = "seq_atelier", allocationSize = 1)
	@Column(name = "id_atelier", updatable = false, nullable = false)
	Integer idAtelier;
	
	@Column(name = "nom_atelier", updatable = true, nullable = false, length = 50)
	String nomAtelier;
	
	@Column(name = "diponible", updatable = true, nullable = false)
	boolean disponible;
	
	@Column(name = "place_disponible", updatable = true, nullable = false)
	int placeDisponible;
	
	@Column(name = "tarif", updatable = true, nullable = false)
	int tarif;
	
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name = "id_type")
	TypeAtelierDao typeAtelierDao;

	@OneToMany(mappedBy = "atelierDao", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<ReservationDao> listeReservationDao;

	public AtelierDao(String nomAtelier, boolean disponible) {
		super();
		this.nomAtelier = nomAtelier;
		this.disponible = disponible;
	}

	
	

}
