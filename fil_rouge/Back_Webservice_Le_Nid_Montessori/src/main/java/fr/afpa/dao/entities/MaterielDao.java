package fr.afpa.dao.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity


@Table(name = "materiel")
public class MaterielDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "materiel_generator")
	@SequenceGenerator(name = "materiel_generator", sequenceName = "seq_materiel", allocationSize = 1)
	@Column(name = "id_materiel", updatable = false, nullable = false)
	Integer idMateriel;
	
	@Column(name = "nom_materiel", updatable = true, nullable = false, length = 50)
	String nomMateriel;
	
	@Column(name = "disponible", updatable = true, nullable = false)
	boolean disponible;

	@OneToMany(mappedBy = "materielDao", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<ReservationDao> listeReservationDao;

	public MaterielDao(String nomMateriel, boolean disponible) {
		super();
		this.nomMateriel = nomMateriel;
		this.disponible = disponible;
	}
	
	
	
}