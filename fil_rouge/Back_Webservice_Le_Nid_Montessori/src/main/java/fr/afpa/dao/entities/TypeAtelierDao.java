package fr.afpa.dao.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table (name="type_atelier")
public class TypeAtelierDao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "type_atelier_generator")
	@SequenceGenerator(name = "type_atelier_generator", sequenceName = "seq_type_atelier",allocationSize = 1)
	@Column (name="id_type", updatable = false, nullable = false )
	Integer id;
	
	@Column (name="type_atelier", updatable = true, nullable = false, length = 50)
	String typeAtelier;
	
	@OneToMany(mappedBy = "typeAtelierDao")
	List<AtelierDao> listeAtelierDao;

	public TypeAtelierDao(String typeAtelier) {
		super();
		this.typeAtelier = typeAtelier;
	}

	
}

