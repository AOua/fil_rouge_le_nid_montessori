package fr.afpa.dao.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Data

@Entity
@Table(name = "role_personne")
public class RoleDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_generator")
	@SequenceGenerator(name = "role_generator", sequenceName = "seq_role", allocationSize = 1)
	@Column(name = "id_role", updatable = false, nullable = false)
	Integer id;

	@Column(name = "libelle", updatable = true, nullable = false, length = 50)
	String libelle;

	@OneToMany(mappedBy = "role",fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<PersonneDao> listePersonnes;

}
