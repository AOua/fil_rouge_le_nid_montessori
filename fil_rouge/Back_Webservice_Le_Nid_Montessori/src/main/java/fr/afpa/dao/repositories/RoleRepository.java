package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.dao.entities.RoleDao;

public interface RoleRepository extends JpaRepository<RoleDao, Integer> {

}
