package fr.afpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.dao.entities.PersonneDao;

@Repository
public interface PersonneRepository extends JpaRepository<PersonneDao, Integer> {

}
