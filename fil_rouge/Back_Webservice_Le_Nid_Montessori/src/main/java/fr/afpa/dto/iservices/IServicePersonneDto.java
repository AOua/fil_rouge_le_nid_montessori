package fr.afpa.dto.iservices;

import java.util.List;

import fr.afpa.metier.entities.Personne;

public interface IServicePersonneDto {
	
	public List<Personne> findAll();

	public Personne findOne(Integer id);

	public Personne addPersonne(Personne personne);

	public Personne activerPersonne(Personne personne);	


}
