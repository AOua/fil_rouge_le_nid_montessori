package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.entities.MaterielDao;
import fr.afpa.dao.repositories.MaterielRepository;
import fr.afpa.dto.iservices.IServiceMaterielDto;
import fr.afpa.metier.entities.Materiel;

@Service
public class ServiceMaterielDto implements IServiceMaterielDto {

	@Autowired
	private MaterielRepository materielRepository;
	

	@Override
	public List<Materiel> findAll() {
		List<MaterielDao> listeMaterielDao = materielRepository.findAll();
		List<Materiel> listeMateriel = null;
		if (listeMaterielDao != null) {
			listeMateriel = listMaterielDaoToListMaterielMetier(listeMaterielDao);
		}
		return listeMateriel;
	}

	private List<Materiel> listMaterielDaoToListMaterielMetier(List<MaterielDao> listeMaterielDao) {
		List<Materiel> listMateriel = new ArrayList<Materiel>();

		for (MaterielDao materielDao : listeMaterielDao) {
			Materiel materiel = materielDaoToMaterielMetier(materielDao);
			listMateriel.add(materiel);
		}

		return listMateriel;
	}

	private Materiel materielDaoToMaterielMetier(MaterielDao materielDao) {
		Materiel materiel = new Materiel();
		materiel.setIdMateriel(materielDao.getIdMateriel());
		materiel.setNomMateriel(materielDao.getNomMateriel());
		materiel.setDisponible(materielDao.isDisponible());
		return materiel;
	}

	@Override
	public Materiel findOne(Integer id) {
		Optional<MaterielDao> materielDao = materielRepository.findById(id);
		Materiel materiel = null;
		materiel = materielDaoToMaterielMetier(materielDao.get());
		if (materielDao.isPresent()) {
		return materiel;
		}
		return null;
	}

	@Override
	public Materiel addMateriel(Materiel materiel) {
		MaterielDao materielDao = materielMetierToMaterielDao(materiel);

		 materielRepository.save(materielDao);
		 
		return materiel;
	}

	private MaterielDao materielMetierToMaterielDao(Materiel materiel) {
		MaterielDao materielDao = new MaterielDao();
		materielDao.setIdMateriel(materiel.getIdMateriel());
		materielDao.setNomMateriel(materiel.getNomMateriel());
		materielDao.setDisponible(materiel.isDisponible());
		return materielDao;
	}
}
