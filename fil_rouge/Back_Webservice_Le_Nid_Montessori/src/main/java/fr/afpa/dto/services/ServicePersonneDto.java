package fr.afpa.dto.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.entities.AuthentificationDao;
import fr.afpa.dao.entities.PersonneDao;
import fr.afpa.dao.entities.RoleDao;
import fr.afpa.dao.repositories.PersonneRepository;
import fr.afpa.dto.iservices.IServicePersonneDto;
import fr.afpa.metier.entities.Personne;

@Service
public class ServicePersonneDto implements IServicePersonneDto {

	@Autowired
	private PersonneRepository personneRepository;
	
	@Override
	public List<Personne> findAll() {
		List<PersonneDao> listePersDao = personneRepository.findAll();
		List<Personne> listePers = null;
		if (listePersDao != null) {
			listePers = listPersonneDaoToListPersonneMetier(listePersDao);
		}
		return listePers;
	}

	public List<Personne> listPersonneDaoToListPersonneMetier(List<PersonneDao> listePersDao) {
			List<Personne> listPersonne = new ArrayList<Personne>();

			for (PersonneDao personneDao : listePersDao) {
				Personne personne = personneDaoToPersonneMetier(personneDao);
				listPersonne.add(personne);
			}

			return listPersonne;
	}

	public static Personne personneDaoToPersonneMetier(PersonneDao personneDao) {
		Personne personne = new Personne();
		personne.setId(personneDao.getId());
		personne.setNom(personneDao.getNom());
		personne.setPrenom(personneDao.getPrenom());
		personne.setDateDeNaissance(personneDao.getDateDeNaissance());
		personne.setAdresse(personneDao.getAdresse());
		personne.setTel(personneDao.getTel());
		personne.setMail(personneDao.getMail());
		personne.setActif(personneDao.isActif());
		personne.setAuthentification(ServiceAuthentificationDto.authDaoToAuthMetier(personneDao.getAuthentification()));
		personne.setRole(ServiceRoleDto.roleDaoToRoleMetier(personneDao.getRole()));

		return personne;
	}

	private PersonneDao personneMetierToPersonneDao(Personne personne) {
		PersonneDao personneDao = new PersonneDao();
		personneDao.setId(personne.getId());
		personneDao.setNom(personne.getNom());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setDateDeNaissance(personne.getDateDeNaissance());
		personneDao.setAdresse(personne.getAdresse());
		personneDao.setTel(personne.getTel());
		personneDao.setMail(personne.getMail());
		personneDao.setActif(personne.isActif());
		personneDao.setAuthentification(ServiceAuthentificationDto.authMetierToAuthDao(personne.getAuthentification()));
		personneDao.setRole(ServiceRoleDto.roleMetierToRoleDao(personne.getRole()));
		return personneDao;
	}

	@Override
	public Personne findOne(Integer id) {
		Optional<PersonneDao> personneDao = personneRepository.findById(id);
		Personne per = null;
		per = personneDaoToPersonneMetier(personneDao.get());
		if (personneDao.isPresent()) {
		return per;
		}
		return null;
	}

	@Override
	public Personne addPersonne(Personne personne) {
		PersonneDao personneDao = personneMetierToPersonneDao(personne);
		RoleDao roleDao = ServiceRoleDto.roleMetierToRoleDao(personne.getRole());
		AuthentificationDao authentificationDao = ServiceAuthentificationDto.authMetierToAuthDao(personne.getAuthentification());
		personneDao.setRole(roleDao);
		personneDao.setAuthentification(authentificationDao);
		authentificationDao.setPersonne(personneDao);

		 personneRepository.save(personneDao);
		 
		return personne;
	}

	@Override
	public Personne activerPersonne(Personne personne) {
		Optional<PersonneDao> personneDao = personneRepository.findById(personne.getId());

		personneDao.get().setActif(!personne.isActif());
		
		personneRepository.saveAndFlush(personneDao.get());
		
		return personne;
	}

}
