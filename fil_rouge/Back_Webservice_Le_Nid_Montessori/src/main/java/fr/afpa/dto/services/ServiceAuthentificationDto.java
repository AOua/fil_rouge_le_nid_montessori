package fr.afpa.dto.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dao.entities.AuthentificationDao;
import fr.afpa.dao.repositories.AuthentificationRepository;
import fr.afpa.dto.iservices.IServiceAuthentificationDto;
import fr.afpa.metier.entities.Authentification;
import fr.afpa.metier.entities.Personne;

@Service
public class ServiceAuthentificationDto implements IServiceAuthentificationDto {

	@Autowired
	private AuthentificationRepository authDao;

	/**
	 * methode qui permet de transformer une entité authentification métier en
	 * entité authentificationDao
	 * 
	 * @param auth : entité authentification métier à transformer
	 * @return : une entité authentificationDao correspondante à l'entité
	 *         authentification métier
	 */

	public static AuthentificationDao authMetierToAuthDao(Authentification auth) {
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		return authDao;
	}

	/**
	 * méthode qui permet de transformer une entité authentificationDao en entité
	 * authentification métier
	 * 
	 * @param auth : entité authentificationDao à transformer
	 * @return : une entité authentification métier correspondante à l'entité
	 *         authentificationDao
	 */
	public static Authentification authDaoToAuthMetier(AuthentificationDao authDao) {
		Authentification auth = new Authentification(authDao.getLogin(), authDao.getMdp());

		return auth;
	}

	@Override public Personne authentification(String login, String mdp) { 
		//  TODO Auto-generated method stub 
		return null; 
		}

}
