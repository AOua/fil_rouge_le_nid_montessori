package fr.afpa.dto.services;

import org.springframework.stereotype.Service;

import fr.afpa.dao.entities.RoleDao;
import fr.afpa.dto.iservices.IServiceRoleDto;
import fr.afpa.metier.entities.Role;

@Service
public class ServiceRoleDto implements IServiceRoleDto {


	public static RoleDao roleMetierToRoleDao(Role role) {
		RoleDao roleDao = new RoleDao();
		roleDao.setId(role.getId());
		roleDao.setLibelle(role.getLibelle());

		return roleDao;
	}

	public static Role roleDaoToRoleMetier(RoleDao roleDao) {
		Role role = new Role(roleDao.getId(), roleDao.getLibelle());
		return role;
	}

}
