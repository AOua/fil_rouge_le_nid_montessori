package fr.afpa.dto.iservices;

import java.util.List;

import fr.afpa.metier.entities.Materiel;

public interface IServiceMaterielDto {

	public List<Materiel> findAll();

	public Materiel findOne(Integer id);

	public Materiel addMateriel(Materiel materiel);
}
