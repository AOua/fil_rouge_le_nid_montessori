package fr.afpa.dto.iservices;

import fr.afpa.metier.entities.Personne;

public interface IServiceAuthentificationDto {

	public Personne authentification(String login, String mdp) ;
	
}
