import React, { Component } from "react";

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Connexion from "./Page/Connexion";
import Inscription from "./Page/Inscription";
import AccueilAdmin from "./Page/AccueilAdmin";
import ListerMateriel from "./Page/ListerMateriel";

import "./index.css";
import "./App.css";

class App extends Component {
  render() {
    
    return (
      <div>
        <Router>          
          <Switch>
          <Route path="/inscription">
           <Inscription/>
          </Route>
          <Route path="/accueilAdmin">
           <AccueilAdmin/>
          </Route>
          <Route path="/listerMateriel">
           <ListerMateriel/>
          </Route>
          <Route path="/">
           <Connexion/>
          </Route>
        </Switch>
        </Router>        
      </div>
    );
  }
}

export default App;
