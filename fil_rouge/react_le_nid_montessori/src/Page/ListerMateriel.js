import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink } from 'mdbreact';

export default function ListerMateriel() {

    const [materiels, setMateriels] = useState([]);

    const getMateriels = () => {

        fetch(`http://localhost:8080/materiels`, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })

            .then(res => {
                return res.json();
            })

            .then(data => {

                setMateriels(data);

            })

            .catch(error => {
                console.log(error);

            })
    }
    useEffect(() => {

        const fetchData = async () => {

            await getMateriels();
        }
        fetchData()

    }, []);

    const materielsList = materiels.map((materiel) =>
        <tr key={materiel.id}>
            <td>{materiel.nomMateriel}</td>
            <td>{(materiel.disponible).toString()}</td>
            <td>
                <div className="btn-group" role="group">
                    <Link to="#"><button className="btn btn-light" type="submit">Modifier</button></Link>
                    <input type="submit" className="btn btn-danger" name="supprimer" value="supprimer" />
                </div>
            </td>
        </tr>);

    return <div >
        <div>
            <MDBNavbar className="navCss" dark expand="md">
                <MDBNavbarBrand>
                    <strong>Bienvenue</strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler />
                <MDBCollapse navbar>
                    <MDBNavbarNav left>
                        <MDBNavItem active>
                            <MDBNavLink to="/accueiladmin">Gestion des utilisateurs</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="#">Gestion des ateliers</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="/listermateriel">Gestion du matériel</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>

                    <MDBNavbarNav right>
                        <MDBNavItem>
                            <MDBNavLink to="/Connexion">Se déconnecter</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>
                </MDBCollapse>
            </MDBNavbar>
        </div>
        <div>

            <div>
                <h4 className="title-form">Liste du matériel : </h4>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nom</th>
                        <th scope="col">Disponible</th>
                    </tr>
                </thead>
                <tbody>
                    {materielsList}
                </tbody>
            </table>
        </div>
        <div className="creation-materiel">
            <div>
                <h4 className="title-form">Ajouter un matériel</h4>
            </div>
            <form >
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <label htmlFor="nom">Nom :</label>
                        <input type="text" className="form-control" id="nom" name="nom" />
                    </div>
                    <div className="form-group col-md-6">
                        <input type="submit" className="btn btn-primary" name="ajouter" value="Valider" /* onClick={() => { AjoutMateriel() }} */ />
                    </div>
                </div>
            </form>
        </div>
    </div>
}

function AjoutMateriel() {
    const [materiel, setMateriel] = useState({
        "id": 0,
        "nom": "",
        "disponible": true
      });

    
      function changeHandler(event) {
        setMateriel(materiel)
      };

      async function submitForm(event) {
        event.preventDefault();
        fetch(`http://localhost:8080/materiels`, {
          method: "POST",
    
          body: JSON.stringify(materiel),
    
          headers: {
            'Accept': 'application/json',
            "Content-type": "application/json; charset=UTF-8",
          }
        })
    
        .then(res => {

            if (res.status === 200) {

            }
            return res.text();


        })
        .catch(error => {
            console.log(error);
        })
    window.location.href = "/listermateriel";
    
    
    }
}


