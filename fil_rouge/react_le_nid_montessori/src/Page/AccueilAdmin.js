import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink } from 'mdbreact';

export default function AccueilAdmin() {

    const [users, setUsers] = useState([]);

    const getUsers = () => {

        fetch(`http://localhost:8080/personnes`, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })


            .then(res => {
                return res.json();
            })

            .then(data => {

                setUsers(data);

            })

            .catch(error => {
                console.log(error);

            })
    }
    useEffect(() => {

        const fetchData = async () => {

            await getUsers();
        }
        fetchData()

    }, []);

    const usersList = users.map((user) =>
        <tr key={user.id}>
            <td>{user.nom}</td>
            <td>{user.prenom}</td>
            <td>{user.tel}</td>
            <td>{user.mail}</td>
            <td>
                <div className="btn-group" role="group">
                    <input type="submit" className="btn btn-primary" name="activer" value="activer" onClick={() => { activerUser(user) }} />
                    <Link to="#"><button className="btn btn-light" type="submit">Modifier</button></Link>
                    <input type="submit" className="btn btn-danger" name="supprimer" value="supprimer" onClick={() => { deleteUser(user.id) }} />
                </div>
            </td>
        </tr>);

    return <div >
        <div>
            <MDBNavbar className="navCss" dark expand="md">
                <MDBNavbarBrand>
                    <strong>Bienvenue</strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler />
                <MDBCollapse navbar>
                    <MDBNavbarNav left>
                        <MDBNavItem active>
                            <MDBNavLink to="/accueiladmin">Gestion des utilisateurs</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="#">Gestion des ateliers</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="/listermateriel">Gestion du matériel</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="#">Gestion des réservations</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>

                    <MDBNavbarNav right>
                        <MDBNavItem>
                            <MDBNavLink to="/connexion">Se déconnecter</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>
                </MDBCollapse>
            </MDBNavbar>
        </div>
        <div>
            <h4 className="title-form">Liste des utilisateurs : </h4>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Prenom</th>
                    <th scope="col">Tel</th>
                    <th scope="col">Mail</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {usersList}
            </tbody>
        </table>
    </div>
}

function deleteUser(id) {
    fetch(`http://localhost:8080/personnes/${id}`, {

        method: 'delete'
    })
        .then(res => {

            if (res.status === 200) {

            }
            return res.text();


        })
        .catch(error => {
            console.log(error);
        })
        alert('Utilisateur supprimé');
    window.location.href = "/accueilAdmin";

}

function activerUser(user) {
    fetch(`http://localhost:8080/personnes`, {

        method: 'put'
    })
        .then(res => {

            if (res.status === 200) {

            }
            return res.text();


        })
        .catch(error => {
            console.log(error);
        })
        alert('Utilisateur activé');
    window.location.href = "/accueilAdmin";

}