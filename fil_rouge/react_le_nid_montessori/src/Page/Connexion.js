import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBLink } from 'mdbreact';

const FormPage = () => {
  return (
    <div>
      <MDBContainer>
        <MDBRow>
          <MDBCol md="6" className="authentification">
            <form>
              <div>
                <MDBRow>
                  <MDBCol>
                    <img src={require('../img/lenidmontessori.png')} id="nom-asso" className="img-fluid" alt="nom asso" />
                  </MDBCol>
                </MDBRow>
              </div>
              <div className="pink-text">
                <MDBInput label="Entrez votre login" icon="user" group type="text" validate error="wrong"
                  success="right" />
                <MDBInput label="Entrez votre mot de passe" icon="lock" group type="password" validate />
              </div>
              <div className="text-center">
                <MDBBtn href="/accueilAdmin" color="pink">Se connecter</MDBBtn>
              </div>
              <div className="text-center" >
                <div className="text-center" >
                <MDBLink className="small" to="/inscription">Vous n’avez pas de compte ? Inscrivez-vous</MDBLink>
              </div>
              </div>
            </form>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </div>
  );
};

export default FormPage;