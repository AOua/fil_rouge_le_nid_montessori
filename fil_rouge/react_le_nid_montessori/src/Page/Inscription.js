import React from "react";

export default class CreationUtilisateur extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      id: 0,
      nom: '',
      prenom: '',
      tel: '',
      dateDeNaissance: '',
      mail: '',
      adresse: '',
      actif: false,
      role: {
        idRole: 2,
        libelle: 'utilisateur'
      },
      authentification: {
        login: '',
        mdp: ''
      }
    }


  }

  
  handleUserInput (e) {
    const name = e.target.name;
    const value = e.target.value;
    if(name === "utilisateur"){
      this.setState({ role: {
            idRole : value
          }
        });      
    }
    
    else if(name === "login"){
      this.setState(
        prevState => ({
          authentification: {
            ...prevState.authentification,
            login : value
          }
        }));
    }else if(name === "mdp"){    
      this.setState(
        prevState => ({
          authentification: {
            ...prevState.authentification,
            mdp : value
          }
        }));
      }
    else{
      this.setState({[name]: value});  
      
    }
    console.log("id :" + this.state.role.idRole);
    console.log("type :" + this.state.type);
  }
  

  handleSubmit = (event) => {
    event.preventDefault();
    
    fetch(`http://localhost:8080/personnes`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json'},
      body: JSON.stringify(this.state),
    }).then(function(response){
      if(response.status === 200){
          alert('Votre inscription a bien été prise en compte, votre espace sera accessible dans 24 heures.');
          window.location.href = "/";
      }
      });
  }


  render() {
    return (
    <div className="bg">
      <div className="formulaire-inscription">
        <div className="title-form">
        <img src={require('../img/lenidmontessori.png')} id="nom-asso" className="img-fluid" alt="nom asso" />
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="nom">Nom :</label>
              <input type="text" className="form-control" id="nom" required pattern="[A-Za-z ]{1,20}" name="nom" value={`${this.state.nom}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="prenom">Prenom :</label>
              <input type="text" className="form-control" required pattern="[A-Za-z ]{1,20}" id="prenom"  name="prenom" value={`${this.state.prenom}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="tel">Date de Naissance :</label>
              <input type="date"id="dateDeNaissance" name="dateDeNaissance" value={`${this.state.dateDeNaissance}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
          </div>
          <div className="form-row">
          <div className="form-group col-md-6">
              <label htmlFor="tel">Telephone :</label>
              <input type="tel" className="form-control" id="tel" required pattern="[0-9]{10}" name="tel" value={`${this.state.tel}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="email">Email</label>
              <input type="email" className="form-control" required  id="mail" name="mail" value={`${this.state.mail}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-12">
              <label htmlFor="adresse">Adresse</label>
              <input type="text" className="form-control" id="adresse" required name="adresse" value={`${this.state.adresse}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
          </div>

          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="login">Login</label>
              <input type="text" className="form-control" id="login" required name="login" value={`${this.state.authentification.login}`} onChange={(event) => this.handleUserInput(event)}/>
            </div>
            <div className="form-group col-md-6">
              <label htmlFor="password">Mot de passe</label>
              <input type="password" className="form-control" required id="password" name="mdp" value={`${this.state.authentification.mdp}`}  onChange={(event) => this.handleUserInput(event)}/>
            </div>
          </div>
          <button type="submit" className="btn btn-primary btn-form-create">
            Valider
          </button>
        </form>
      </div>
    </div>
    )
  };
}
